## Blackjack for arduino

>>>
Blackjack is a popular American casino game, now found throughout the world. It is a banking
game in which the aim of the player is to achieve a hand whose points total nearer to 21 than the
banker's hand, but without exceeding 21. Blackjack is played with an international 52-card deck
without jokers.

It's fundamentally a two-player game. The player plays against the dealer.


The players' turn

The player can keep his hand as it is (stand) or take more cards from the deck (hit), one at a time,
until either the player judges that the hand is strong enough to go up against the dealer's hand and
stands, or until it goes over 21, in which case the player immediately loses (busts).


The dealer's turn

The dealer takes more cards or stands depending on the value of the hand. Contrary to the player,
though, the dealer's action is completely dictated by the rules. The dealer must take more cards if
the value of the hand is lower than 17, otherwise the dealer will stand.


Cards value
- Cards 2-10 are valued at the face value of the card.
- Face cards such as the King, Queen, and Jack are valued at 10 each.
- The Ace card, however, is a special card and which be valued either at 11 or 1. 

You need Arduino, LCD-display and some switches or push buttons to implement this game. Remember your deck of cards should be normal (4 aces, 12 face cards etc..). You should see on the display yours and dealers hand, and Arduino should announce the winner.
>>>
