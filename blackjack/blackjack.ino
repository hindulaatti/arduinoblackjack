/*
===================================================================
Name          :      Just a regular blackjack
Author        :      Jimi Toiviainen
Description   :      Just a regular blackjack
===================================================================
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <LiquidCrystal.h>
#define NUM_CARDS 52
#define HAND_SIZE 11 // Max number of cards = 1+1+1+1+2+2+2+2+3+3+3 = 21
#define array_length(x)  (sizeof(x) / sizeof((x)[0])) // To find out size of arrays

// initialize the library by associating any needed LCD interface pin
// with the arduino pin number it is connected to
const int rs = 12, en = 11, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

// constants won't change. They're used here to set pin numbers:
const int hitButtonPin = 6;    // the number of the pushbutton pin
const int standButtonPin = 7;    // the number of the pushbutton pin
const int buttons[] = {hitButtonPin, standButtonPin};
int buttonPressed = 0;

// Init arrays
int reading[ array_length(buttons) ];
int buttonState[ array_length(buttons) ];
int lastButtonState[ array_length(buttons) ];
unsigned long lastDebounceTime[ array_length(buttons) ]; // the last time the output pin was toggled

// unsigned long because the time, measured in
// milliseconds, will quickly become a bigger number than can be stored in an int.
unsigned long debounceDelay = 50; // the debounce time; increase if the output flickers

void setup() {
  
  // Fill arrays
  for(int i = 0; i < array_length(buttons); i++){
    lastButtonState[i] = LOW;   // the previous reading from the input pin
    lastDebounceTime[i] = 0;  // the last time the output pin was toggled
  }
  Serial.begin(9600);
  pinMode(hitButtonPin , INPUT);
  pinMode(standButtonPin, INPUT);
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
}

// program flow
// welcome the player
// await button press
// initialize deck
// shuffle deck
// call play function
  // initialize dealer hand, only one card
  // initialize player hand, two cards
  // count hand value for dealer
  // count hand value for player
  // print hands to screen
  // check if player has blackjack
  // if not blackjack, await button press
  // hit or stand, press either button
      // if hit
        // deal new card
        // count hand value for player
        // check if player has blackjack
        // check if player bust
        // print hands to screen
  // if game not over yet
  // hit dealer cards until their value > 16
  // show hands
  // print results
        
 
void loop(){
  int deck[NUM_CARDS]; // Deck that has not been shuffled
  int sdeck[NUM_CARDS]; // The shuffled deck
  
  lcd.clear(); 
  delay(2);
  lcd.setCursor(0, 0);
  lcd.print("Welcome");
  lcd.setCursor(0, 1);
  lcd.print("Press any key");
  waitButton(buttons);
  initDeck(deck, sdeck);
  shuffle(deck, sdeck);
  
  play(sdeck);
}

// Play the game with a deck
void play(int *sdeck){
  int cardIndex = 0; // Which card is the next card from the deck
  int playerIndex = 0; // How many cards does the player have (-1)
  int dealerIndex = 0; // How many cards does the dealer have (-1)
  int volatile playerHandValue[2] = {0,0}; // { [hand value], [how many aces in hand] }
  int volatile dealerHandValue[2] = {0,0};
  int gameEnded = 0; // Do we need to run the last part of the program
  char input[6]; // Input from user

  // Initialize player and dealer hands. 99 is empty slot
  int playerHand[HAND_SIZE] = {0};
  for(int i=0; i < HAND_SIZE; i++) playerHand[i]=99;

  int dealerHand[HAND_SIZE] = {0};
  for(int i=0; i < HAND_SIZE; i++) dealerHand[i]=99;
  
  // Deal one for dealer and two for player
  hit(&cardIndex, sdeck, &dealerIndex, dealerHand);
  hit(&cardIndex, sdeck, &playerIndex, playerHand);
  hit(&cardIndex, sdeck, &playerIndex, playerHand);
  
  // Calculate hand values
  handValue(dealerHand, dealerHandValue);
  handValue(playerHand, playerHandValue);

  showHands(dealerHand, dealerHandValue, playerHand, playerHandValue);

  // Check if player has blackjack, and don't continue execution if so
  if (checkBlackJack(playerHandValue)){
    lcd.clear(); 
    delay(2);
    lcd.setCursor(0,0);
    lcd.print("Blackjack!");
    lcd.setCursor(0,1);
    lcd.print("Press any key");
    
    gameEnded = 1;
  }

  // If answer is not s to any input we ask
  while(!gameEnded){
    if (buttonPressed == hitButtonPin){
      
      // If player wants, hit card into his hand
      hit(&cardIndex, sdeck, &playerIndex, playerHand);

      handValue(playerHand, playerHandValue);

      showHands(dealerHand, dealerHandValue, playerHand, playerHandValue);

      // Check if player has blackjack, and don't continue execution if so
      if(checkBlackJack(playerHandValue)){
        lcd.clear(); 
        delay(2);
        lcd.setCursor(0,0);
        lcd.print("Blackjack!");
        lcd.setCursor(0,1);
        lcd.print("Press any key");
        gameEnded = 1;
        break;
      }

      // Check if player already lost, and don't continue execution if so
      if(playerHandValue[0] > 21){
        lcd.clear(); 
        delay(2);
        lcd.setCursor(0,0);
        lcd.print("You lose!");
        lcd.setCursor(0,1);
        lcd.print("Press any key");
        gameEnded = 1;
        break;
      }

    } else {
      break;
    }
  }

  if(!gameEnded){ // If player didn't win or lose and standed

    while(dealerHandValue[0] < 17){ // Hit until dealer has more than 16 value on their cards
      hit(&cardIndex, sdeck, &dealerIndex, dealerHand);
      delay(50);
      handValue(dealerHand, dealerHandValue);
      delay(50);
    }

    showHands(dealerHand, dealerHandValue, playerHand, playerHandValue);
    delay(100);
    printResult(dealerHandValue, playerHandValue); // Check score and print results
    
  }
}

// Returns which button was pressed
int waitButton(int *buttons){

  while(true){
    for(int i = 0; i < array_length(buttons); i++){
      // read the state of the switch into a local variable:
      reading[0] = digitalRead(buttons[0]);
    
      // check to see if you just pressed the button
      // (i.e. the input went from LOW to HIGH), and you've waited long enough
      // since the last press to ignore any noise:
    
      // If the switch changed, due to noise or pressing:
      if (reading[0] != lastButtonState[0]) {
        // reset the debouncing timer
        lastDebounceTime[0] = millis();
      }
    
      if ((millis() - lastDebounceTime[0]) > debounceDelay) {
        // whatever the reading is at, it's been there for longer than the debounce
        // delay, so take it as the actual current state:
    
        // if the button state has changed:
        if (reading[0] != buttonState[0]) {
          buttonState[0] = reading[0];
    
          // only toggle the LED if the new button state is HIGH
          if (buttonState[0] == HIGH) {
            // save the reading. Next time through the loop, it'll be the lastButtonState:
            lastButtonState[0] = reading[0];
            return buttons[0];
            //break;
          }
        }
      }
    
      // save the reading. Next time through the loop, it'll be the lastButtonState:
      lastButtonState[0] = reading[0];
    }
  }
}

// Give the next card from this deck to this hand
void hit(int *cardIndex, int *sdeck, int *index, int *hand){
    int card = sdeck[*cardIndex];
  for(int i=0; i < 10; i++){
    if(hand[i]==99) {
      hand[i]=card;
      break;
    }
  }

  *index += 1;
    *cardIndex += 1;
}

// Check score and print results
void printResult(int *dealerHandValue, int *playerHandValue){
  // Easy way to check with aces too
  int playerAceModifier = 0;
  int dealerAceModifier = 0;

  if(playerHandValue[1] =! 0 && playerHandValue[0] + 10 < 22){
    playerAceModifier = 10;
  }

  if(dealerHandValue[1] =! 0 && dealerHandValue[0] + 10 < 22){
    dealerAceModifier = 10;
  }

  int playerScore = playerHandValue[0] + playerAceModifier;
  int dealerScore = dealerHandValue[0] + dealerAceModifier;
  lcd.clear(); 
  delay(2);
  if(dealerScore > 21){
    lcd.setCursor(0,0);
    lcd.print("Dealer bust!");
    lcd.setCursor(0,1);
    lcd.print("You're winner!");
  } else if(playerScore > dealerScore){
    lcd.setCursor(0,0);
    lcd.print("You're winner!");
  } else if(playerScore == dealerScore){
    lcd.setCursor(0,0);
    lcd.print("Tie!");
    lcd.setCursor(0,1);
    lcd.print("You lose.");
  } else if(playerScore < dealerScore){
    lcd.setCursor(0,0);
    lcd.print("Dealer wins!");
  }
  waitButton(buttons);
}

// Check if the hand has blackjack
int checkBlackJack(int *handValue){
  if(handValue[0] == 21) return 1;
  if(handValue[1] =! 0){
    if(handValue[0] + 10 == 21) return 1;
  }
  return 0;
}

// Calculate the points of the hand
void handValue(int *hand, int *handValue){
  int value[] = { 0, 0 }; // first is value, second is number of aces
  for(int i=0; i < 10; i++){
    if(hand[i]==99) break;
    
    if (cardValue(hand[i]) == 1){
      value[1] += 1;
    }
    value[0] += cardValue(hand[i]);
  }
  handValue[0] = value[0];
  handValue[1] = value[1];
}

// Show both hands and returns the button that was pressed
int showHands(int *dealerHand, int *dealerHandValue, int *playerHand, int *playerHandValue){
  lcd.clear(); 
  delay(2);
  lcd.setCursor(0,0);
  lcd.print("D:");
  printHand(dealerHand);
  printHandValue(dealerHandValue);

  lcd.setCursor(0,1);
  lcd.print("P:");
  printHand(playerHand);
  printHandValue(playerHandValue);
  return waitButton(buttons);
}

// Print the value of a hand
void printHandValue(int *handValue){
  lcd.print(" ");
  if(handValue[1] == 0){
    lcd.print(handValue[0]);
  } else if(handValue[1] > 0 && handValue[0] + 10 < 22){
    lcd.print(handValue[0]);
    lcd.print("|");
    lcd.print(handValue[0] + 10);
  }
}

// Print all cards from a hand
void printHand(int *hand){
  for (int i = 0;i < 10; i++) {
    if (hand[i] == 99) {
      break;
    }
   
    lcd.print(cardFace(hand[i]));
  }
}

// Calculate the value of a card
int cardValue(int card){
    int a;
    if(((card % 13)+1)<10)
        a = card%13+1;
    else 
        a = 10;
    return a;
}

// Get the face of a card
char *cardFace(int card){
  const char* kFaces[] = {
    "A",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "T",
    "J",
    "Q",
    "K"
    };
    return (char *)kFaces[ card % 13 ];
}

// Initialize a deck
void initDeck(int *deck, int *sdeck){
    int i=0;
    for(i;i<52;i++){
        deck[i] = i+1;
    }
}

// Shuffle a deck
void shuffle(int *deck, int *sdeck){
    int randNumber = random(1,53);
  // Copy the unshuffled deck
    for(int i; i < 52; i++){
        sdeck[i] = deck[i];
    }

    for(int i = NUM_CARDS - 1; i > 0; i--){
        int j = randNumber % (i + 1);
        int n = sdeck[i];
        sdeck[i] = sdeck[j];
        sdeck[j] = n;
    }
}
