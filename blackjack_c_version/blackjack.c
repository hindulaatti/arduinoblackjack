/*
===================================================================
Name          :      Just a regular blackjack
Author        :      Jimi Toiviainen
Description   :      Just a regular blackjack
===================================================================
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#define NUM_CARDS 52
#define HAND_SIZE 11 // Max number of cards = 1+1+1+1+2+2+2+2+3+3+3 = 21

void play(int *sdeck);
void hit(int *cardIndex, int *sdeck, int *index, int *hand);
void printResult(int *dealerHandValue, int *playerHandValue);
int checkBlackJack(int *handValue);
void handValue(int *hand, int *handValue);
void showHands(int *dealerHand, int *dealerHandValue, int *playerHand, int *playerHandValue);
void printHandValue(int *handValue);
void printHand(int *hand);
void printDeck(int *deck);
int cardValue(int card);
char *cardFace(int card);
char *cardSuit(int card);
void initDeck(int *deck, int *sdeck);
void shuffle(int *deck, int *sdeck);

int main(){
	int deck[NUM_CARDS]; // Deck that has not been shuffled
	int sdeck[NUM_CARDS]; // The shuffled deck
	
    printf("Welcome\n\n");
	
	initDeck(deck, sdeck);
	shuffle(deck, sdeck);
	
	play(sdeck);
}

// Play the game with a deck
void play(int *sdeck){
	int cardIndex = 0; // Which card is the next card from the deck
	int playerIndex = 0; // How many cards does the player have (-1)
	int dealerIndex = 0; // How many cards does the dealer have (-1)
	int playerHandValue[2] = {0,0}; // { [hand value], [how many aces in hand] }
	int dealerHandValue[2] = {0,0};
	int gameEnded = 0; // Do we need to run the last part of the program
	char input[6]; // Input from user

	// Initialize player and dealer hands. 99 is empty slot
	int playerHand[HAND_SIZE] = {0};
	for(int i=0; i < HAND_SIZE; i++) playerHand[i]=99;

	int dealerHand[HAND_SIZE] = {0};
	for(int i=0; i < HAND_SIZE; i++) dealerHand[i]=99;
	
	// Deal one for dealer and two for player
	hit(&cardIndex, sdeck, &dealerIndex, dealerHand);
	hit(&cardIndex, sdeck, &playerIndex, playerHand);
	hit(&cardIndex, sdeck, &playerIndex, playerHand);
	
	//printDeck(sdeck); if you want to cheat ;)
	
	// Calculate hand values
	handValue(dealerHand, dealerHandValue);
	handValue(playerHand, playerHandValue);

	showHands(dealerHand, dealerHandValue, playerHand, playerHandValue);

	// Check if player has blackjack, and don't continue execution if so
	if(checkBlackJack(playerHandValue)){
		printf("Blackjack!");
		gameEnded = 1;
		break;
	}

	// If answer is not s to any input we ask
	while(strcmp(input, "s") != 0 || gameEnded = 0){
		printf("\nHit or stand? (h or s)");

		scanf("%5s", input);
		if(strcmp(input, "h") == 0){
			
			// If player wants, hit card into his hand
			hit(&cardIndex, sdeck, &playerIndex, playerHand);

			handValue(playerHand, playerHandValue);

			printf("\nYour hand:\n");
			printHand(playerHand);
			printHandValue(playerHandValue);

			// Check if player has blackjack, and don't continue execution if so
			if(checkBlackJack(playerHandValue)){
				printf("Blackjack!");
				gameEnded = 1;
				break;
			}

			// Check if player already lost, and don't continue execution if so
			if(playerHandValue[0] > 21){
				printf("You lose!");
				gameEnded = 1;
				break;
			}

			strcpy(input, ""); // Clean input variable just in case
		} else if(strcmp(input, "s") != 0) {
			printf("wrong input"); // If it isn't h or s it's wrong
		}
	}

	if(!gameEnded){ // If player didn't win or lose and standed

		while(dealerHandValue[0] < 17){ // Hit until dealer has more than 16 value on their cards
			hit(&cardIndex, sdeck, &dealerIndex, dealerHand);
			handValue(dealerHand, dealerHandValue);
		}

		showHands(dealerHand, dealerHandValue, playerHand, playerHandValue);
		printResult(dealerHandValue, playerHandValue); // Check score and print results
	}
}

// Give the next card from this deck to this hand
void hit(int *cardIndex, int *sdeck, int *index, int *hand){
    int card = sdeck[*cardIndex];
	for(int i=0; i < 10; i++){
		if(hand[i]==99) {
			hand[i]=card;
			break;
		}
	}

	*index += 1;
    *cardIndex += 1;
}

// Check score and print results
void printResult(int *dealerHandValue, int *playerHandValue){
	// Easy way to check with aces too
	int playerAceModifier = 0;
	int dealerAceModifier = 0;

	if(playerHandValue[1] =! 0 && playerHandValue[0] + 10 < 22){
		playerAceModifier = 10;
	}

	if(dealerHandValue[1] =! 0 && dealerHandValue[0] + 10 < 22){
		dealerAceModifier = 10;
	}

	int playerScore = playerHandValue[0] + playerAceModifier;
	int dealerScore = dealerHandValue[0] + dealerAceModifier;

	if(dealerScore > 21){
		printf("Dealer bust! You're winner!");
	} else if(playerScore > dealerScore){
		printf("You're winner!");
	} else if(playerScore == dealerScore){
		printf("Tie! You lose.");
	} else if(playerScore < dealerScore){
		printf("Dealer wins!");
	}
}

// Check if the hand has blackjack
int checkBlackJack(int *handValue){
	if(handValue[0] == 21) return 1;
	if(handValue[1] =! 0){
		if(handValue[0] + 10 == 21) return 1;
	}
	return 0;
}

// Calculate the points of the hand
void handValue(int *hand, int *handValue){
	int value[] = { 0, 0 };
	for(int i=0; i < 10; i++){
		if(hand[i]==99) break;
		
		if (cardValue(hand[i]) == 1){
			value[1] += 1;
		}
		value[0] += cardValue(hand[i]);
	}
	handValue[0] = value[0];
	handValue[1] = value[1];
}

// Show both hands
void showHands(int *dealerHand, int *dealerHandValue, int *playerHand, int *playerHandValue){

	printf("\nDealers hand:\n");
	printHand(dealerHand);
	printHandValue(dealerHandValue);

	printf("\nYour hand:\n");
	printHand(playerHand);
	printHandValue(playerHandValue);
}

// Print the value of a hand
void printHandValue(int *handValue){
	if(handValue[1] == 0){
		printf("Value: %d\n", handValue[0]);
	} else if(handValue[1] > 0 && handValue[0] + 10 < 22){
		printf("Value: %d or %d\n", handValue[0], handValue[0] + 10);
	}
}

// Print all cards from a hand
void printHand(int *hand){
	printf("\n");
	for(int i=0;i < 10; i++){
		if(hand[i]==99) break;
		
		if (cardValue(hand[i]) == 1){
			printf("%s of %s, value: %d or %d\n", cardFace(hand[i]), cardSuit(hand[i]), cardValue(hand[i]), cardValue(hand[i])+10);
		} else {
			printf("%s of %s, value: %d\n", cardFace(hand[i]), cardSuit(hand[i]), cardValue(hand[i]));
		}
	}
	printf("\n");
}

// Print all cards in order from a deck
void printDeck(int *deck){
	for(int i=0;i < NUM_CARDS; i++){
		printf("%s of %s\n", cardFace(deck[i]), cardSuit(deck[i]));
	}
}

// Calculate the value of a card
int cardValue(int card){
    int a;
    if(((card%13)+1)<10)
        a = card%13+1;
    else 
        a = 10;
    return a;
}

// Get the face of a card
char *cardFace(int card){
	const char* kFaces[] = {
		"Ace",
		"Two",
		"Three",
		"Four",
		"Five",
		"Six",
		"Seven",
		"Eight",
		"Nine",
		"Ten",
		"Jack",
		"Queen",
		"King"
    };
    return (char *)kFaces[ card % 13 ];
}

// Get the suit of a card
char *cardSuit(int card){
    const char* kSuits[] = {
        "Hearts",
        "Clubs",
        "Diamonds",
        "Spades"
    };
    return (char *)kSuits[ card % 4 ];
}

// Initialize a deck
void initDeck(int *deck, int *sdeck){
    int i=0;
    for(i;i<52;i++){
        deck[i] = i+1;
    }
}

// Shuffle a deck
void shuffle(int *deck, int *sdeck){
    srand(time(NULL));

	// Copy the unshuffled deck
    for(int i; i < 52; i++){
        sdeck[i] = deck[i];
    }

    for(int i = NUM_CARDS - 1; i > 0; i--){
        int j = rand() % (i + 1);
        int n = sdeck[i];
        sdeck[i] = sdeck[j];
        sdeck[j] = n;
    }
}